import { makeObservable, observable, action } from "mobx";

export default class mobxTodo {
  userInput = "";
  filterType = "all";
  items = [];
  editId = null;
  editValue = null;
  constructor() {
    this.that = this;
    makeObservable(this, {
      userInput: observable,
      filterType: observable,
      items: observable,
      editId: observable,
      editValue: observable,
      itemAdd: action,
      inputChange: action,
      setEditId: action,
      setEditValue: action,
      itemCheck: action,
      itemRemove: action,
   
    });
  }
  itemAdd = () => {
    this.userInput = "";
  };
  inputChange = (e) => {
    this.userInput = e.target.value;
  };
  setEditValue = (value) => {
    this.editValue = value;
    console.log(this.editValue);
  };
  itemCheck = (data) => {
    this.items = this.items.map((item) => {
      if (item.id === data.id) {
        return {
          ...item,
          isCompleted: !data.isCompleted,
        };
      } else {
        return item;
      }
    });
  };
  itemRemove = (data) => {
    this.items = this.items.filter((item) => {
      return item.id !== data.id;
    });
    console.log(this.items);
  };
  setFilterType = (value) => {
    this.filterType = value;
  };
  setEditId = (id,name) => {
    console.log(name)
      this.items = this.items.map((item) => {
        if (id === item.id) {
          return {
            ...item,
            name: name,
          };
        }
        return item;
    });
      
  }
  clearComplete = () => {
    console.log("ASDf");
    this.items = this.items.filter((data) => data.isCompleted === false);
  };
}

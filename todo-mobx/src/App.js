import logo from './logo.svg';
import './App.css';
import Todo from './components/Todo';

function App({store}) {
  return (
    <div className="App">
      <Todo store={store}/>
    </div>
 
  );
}

export default App;

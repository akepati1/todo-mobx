import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux';
import mobxTodo from "./mobx/mobxTodo"
const root = ReactDOM.createRoot(document.getElementById('root'));
const store = new mobxTodo();
console.log(store)
root.render(
 
    <App store = {store} />
 
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

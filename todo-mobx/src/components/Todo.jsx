import React from "react";
import Element from "./Element";
import { observer } from "mobx-react-lite";
import { useMemo } from "react";
import "../todo.css";
const Todo = observer(({ store }) => {
  const {
    items,
    filterType,
    userInput,
    editId,
    itemAdd,
    inputChange,
    itemCheck,
    itemRemove,
    setFilterType,
    setEditId,
    clearComplete,
  } = store;

  console.log(items);
  const handleAdd = () => {
    let obj = {
      id: Math.round(Math.random() * 100), // UUID
      name: userInput,
      isCompleted: false,
    };
    items.push(obj);
    itemAdd();
  };
  let leftItems = useMemo(
    () =>
      items.filter((item) => {
        return item.isCompleted === false;
      }).length,
    [items]
  );

  let displayArray = useMemo(() => {
    if (filterType === "Active") {
      return items.filter((item) => item.isCompleted === false);
    } else if (filterType === "Completed") {
      return items.filter((item) => item.isCompleted === true);
    } else {
      return items;
    }
  }, [items, filterType]);
  return (
    <div className="container-todo">
      <p>
        TODO-LIST <span>Mobx</span>
      </p>
     
      <div className="todo">
        <div className="input-add">
          <input
            className="input"
            type="text"
            value={userInput}
            onChange={(e) => inputChange(e)}
            placeholder="What needs to done?"
          ></input>
          <button onClick={handleAdd}>Add Task</button>
        </div>
        {displayArray.map((item, index) => {
          return (
            <Element
              items={store.items}
              editInputValue={store.editValue}
              setEditId={store.setEditId}
              editId={editId}
              data={item}
              toggle={itemCheck}
              onDelete={itemRemove}
              editValue={setEditId}
            />
          );
        })}
        <div className="options">
          <div className="items-left">
            <span>{leftItems}</span> items left
          </div>
          <div className="filters">
            <button
              className={filterType === "All" ? "border-filter" : ""}
              onClick={() => setFilterType("ALL")}
            >
              All
            </button>
            <button
              className={filterType === "Active" ? "border-filter" : ""}
              onClick={() => setFilterType("Active")}
            >
              Active
            </button>
            <button
              className={filterType === "Completed" ? "border-filter" : ""}
              onClick={() => setFilterType("Completed")}
            >
              Completed
            </button>
          </div>
          <div onClick={clearComplete}>Clear-completed</div>
        </div>
      </div>
    </div>
  );
});

export default Todo;

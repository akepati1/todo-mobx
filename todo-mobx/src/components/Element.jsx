import React from "react";
import { useState,useEffect } from "react";
import "../elements.css";
import { observer } from "mobx-react-lite";
const Element = observer((props) => {
  let [updateValue, setUpdateValue] = useState("");
  const [saveId, setSaveId] = useState(null);
  const { id, name, isCompleted } = props.data || {};
  const {
    toggle,
    setEditId,
    onDelete,
    data,
    
  } = props;
  useEffect(() => {
    setUpdateValue(name)
  },[])
  const handleEdit = (id, value) => {
    setSaveId(id);
    if (saveId) {
      saveValue(id, value);
    }
  };
  const saveValue = (id, updateValue) => {
    console.log(updateValue);
    if (id === saveId) {
      setEditId(id, updateValue);
    }
    setSaveId(null);
  };

  return (
    <div className="container">
      <div
        onClick={() => toggle(data)}
        className={isCompleted === true ? "check-completed" : "check"}
      ></div>

      <li className={isCompleted === true ? "strick" : ""}>
        {id != saveId ? (
          name
        ) : (
          <input
            className="input editInput"
            type="text"
            value={updateValue}
            onChange={(e) => setUpdateValue(e.target.value)}
          ></input>
        )}
      </li>

      <button
        style={{ border: "none" }}
        name="edit"
        className={id === saveId ? "save" : "edit"}
        onClick={() => handleEdit(id, updateValue)}
      >
        {id === saveId ? "SAVE" : "EDIT"}
      </button>
      <div onClick={() => onDelete(data)}>X</div>
    </div>
  );
});

export default Element;
